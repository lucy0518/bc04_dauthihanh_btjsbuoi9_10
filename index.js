const DSNV_LOCAlSTORAGE = "DSNV_LOCAlSTORAGE";

let dsnv = [];

let dsnvJson = localStorage.getItem("DSNV");

if (dsnvJson != null) {
  console.log("yes");
  dsnv = JSON.parse(dsnvJson);
  for (var index = 0; index < dsnv.length; index++) {
    let sv = dsnv[index];

    dsnv[index] = new NhanVien(
      taiKhoanNv,
      tenNv,
      email,
      matKhau,
      ngayLam,
      luongCoBan,
      chucVu,
      gioLamTrongThang
    );
  }
  renderDSNV(dsnv);
}

function themNV() {
  var newNV = layThongTinTuForm();

  var isValid =
    validator.kiemTraTaiKhoan(
      newNV.taiKhoan,
      "spanTaiKhoanNV",
      "Tài khoản không được để rỗng",
      "Tài khoản tối đa 4 - 6 ký số"
    ) &
    validator.kiemTraNhanVien(
      newNV.ten,
      "spanTenNV",
      " Tên nhân viên là chữ",
      "Tên nhân viên không được để rỗng "
    ) &
    validator.kiemTraEmail(
      newNV.taiKhoan,
      "spanTaiKhoanNV",
      " Email nhân viên khong được để rỗng",
      4,
      6
    );

  validation.kiemTraMatKhau(
    newNV.matKhau,
    "spanMatKhau",
    "Mật khẩu không được rỗng",
    6,
    10,
    "Mật khẩu từ 6-10 ký tự"
  ) &
    validation.kiemTraNgay(
      newNV.ngayLam,
      "tbNgay",
      "Ngày làm không được rỗng",
      "Vui lòng viết theo định dạng: mm/dd/yyyy"
    ) &
    validation.kiemTraLuong(
      newNV.luongCoBan,
      "tbLuongCB",
      "Lương nhân viên không được rỗng",
      "Lương phải là số",
      1000000,
      20000000,
      "Lương cơ bản 1 000 000 - 20 000 000"
    ) &
    validation.kiemTraChucVu(
      newNV.chucVu,
      "tbChucVu",
      "Vui lòng chọn chức vụ"
    ) &
    validation.kiemTraGioLam(
      newNV.gioLamTrongThang,
      "tbGiolam",
      "Giờ làm không được rỗng",
      "Giờ làm phải là số",
      80,
      200,
      "Số giờ làm trong tháng 80 - 200 giờ"
    );

  if (isValid) {
    dsnv.push(newNV);

    const uniqueIds = [];

    const unique = dsnv.filter((element) => {
      const isDuplicate = uniqueIds.includes(element.taiKhoanNv);

      if (!isDuplicate) {
        uniqueIds.push(element.taiKhoanNv);

        return true;
      }

      return false;
    });

    // tạo json
    var dsnvJson = JSON.stringify(unique);

    localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
    renderDSNV(unique);
  }
  resetInfoInForm();
  document.getElementById(`btnDong`).click();
}

function xoaNhanVien(id) {
  console.log("id: ", id);

  var index = findAcc(id, dsnv);
  console.log(index);

  // tìm thấy vị trí
  if (index != -1) {
    dsnv.splice(index, 1);

    var dsnvJson = JSON.stringify(dsnv);

    localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
    renderDSNV(dsnv);
  }
}

function suaNhanVien(id) {
  document.getElementById(`btnThem`).click();

  var index = findAcc(id, dsnv);
  window["sua_id"] = id;
  console.log("index: ", index);
  if (index != -1) {
    var nv = dsnv[index];
    showInfoToForm(nv);
  }
}

function capNhatNV() {
  const nvUpdate = getInfoFromForm();

  const id = nvUpdate.taiKhoanNv;

  var index = findAcc(id, dsnv);
  if (index != -1) {
    const dataFilter = dsnv.filter((f) => f.acc !== id);
    dataFilter.push(nvUpdate);
    console.log("dataFilter: ", dataFilter);

    //save localstorage

    const dataFilterJson = JSON.stringify(dataFilter);

    localStorage.setItem("DSNV_LOCALSTORAGE", dataFilterJson);

    renderDSNV(dataFilter);
  }

  location.reload();
}

function loaiNV(type) {
  let typeInput = getTypeFromSearch(type);
  console.log("typeInput: ", typeInput);

  typeInput = typeInput.toLowerCase();

  let found = dsnv.filter((f) => f.type().includes(typeInput));
  console.log("found: ", found);
  renderDSNV(found);
}
