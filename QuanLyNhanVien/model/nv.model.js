function NhanVien(
  taiKhoanNv,
  tenNv,
  email,
  matKhau,
  ngayLam,
  luongCoBan,
  chucVu,
  gioLamTrongThang
) {
  this.taiKhoanNv = taiKhoanNv;
  this.name = tenNv;
  this.email = email;
  this.matKhau = matKhau;
  this.ngayLam = ngayLam;
  this.luongCoBan = luongCoBan;
  this.chucVu = chucVu;
  this.gioLamTrongThang = gioLamTrongThang;
  this.tinhTongLuong = function () {
    if (this.chucVu == "Sếp") {
      return this.luongCoBan * 3;
    } else if (this.chucVu == "Trưởng phòng") {
      return this.luongCoBan * 2;
    } else if (this.chucVu == "Nhân viên") {
      return this.luongCoBan;
    }
  };
  this.type = function () {
    if (this.gioLamTrongThang >= 192) {
      return `nhân viên xuất sắc`;
    } else if (this.gioLamTrongThang >= 176) {
      return `nhân viên giỏi`;
    } else if (this.gioLamTrongThang >= 160) {
      return `nhân viên khá`;
    } else {
      return `nhân viên trung bình`;
    }
  };
}
