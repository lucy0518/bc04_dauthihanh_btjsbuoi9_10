var validator = {
  kiemTraTaiKhoan: function (value, idError, message1, message2, min, max) {
    if (value.length == 0) {
      document.getElementById(idError).innerText = message1;
      return false;
    } else if (value.length < min || value.length > max) {
      document.getElementById(idError).innerText = message2;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },

  kiemTraNhanVien: function (value, idError, message1, message2) {
    const reLetter =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    if (value.length == 0) {
      document.getElementById(idError).innerText = message1;
      return false;
    } else if (!reDate.test(value)) {
      document.getElementById(idError).innerText = message2;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },

  kiemTraLuong: function (
    value,
    idError,
    message1,
    message2,
    min,
    max,
    message3
  ) {
    const reNumber = /^[0-9]*$/;

    if (value.length == 0) {
      document.getElementById(idError).innerText = message1;
      return false;
    } else if (!reNumber.test(value)) {
      document.getElementById(idError).innerText = message2;
      return false;
    } else if (value < min || value > max) {
      document.getElementById(idError).innerText = message3;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },

  kiemTraChucVu: function (text, idError, message) {
    if (text == "Chọn chức vụ") {
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },

  kiemTraGioLam: function (
    value,
    idError,
    message1,
    message2,
    min,
    max,
    message3
  ) {
    const reNumber = /^[0-9]*$/;

    if (value.length == 0) {
      document.getElementById(idError).innerText = message1;
      return false;
    } else if (!reNumber.test(value)) {
      document.getElementById(idError).innerText = message2;
      return false;
    } else if (value < min || value > max) {
      document.getElementById(idError).innerText = message3;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
};
