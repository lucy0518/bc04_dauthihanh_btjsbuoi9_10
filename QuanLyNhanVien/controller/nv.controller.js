function layThongTinTuForm() {
  const taiKhoanNv = document.getElementById("TaiKhoanNV").value;
  const tenNv = document.getElementById("TenNV").value;
  const email = document.getElementById("Email").value;
  const matKhau = document.getElementById("Passwork").value;
  const ngayLam = document.getElementById("NgayLam").value;
  const luongCoBan = document.getElementById("LuongCoBan").value;
  const chucVu =
    document.getElementById("ChucVu").options[
      document.getElementById("ChucVu").selectedIndex
    ].text;
  const gioLamTrongThang = document.getElementById("gioLam").value;

  let nv = new NhanVien(
    taiKhoanNv,
    tenNv,
    email,
    matKhau,
    ngayLam,
    luongCoBan,
    chucVu,
    gioLamTrongThang
  );

  console.log(nv.chucVu);

  return nv;
}

function getTypeFromSearch(type) {
  return (type = document.getElementById("searchName").value);
}
function renderDSNV(nvArr) {
  var contentHTML = "";
  for (var i = 0; i < nvArr.length; i++) {
    var nv = nvArr[i];

    let trList = ` <tr>
  
     <td>${nv.taiKhoan}</td>
     <td>${nv.ten}</td>
     <td>${nv.email}</td>
     <td>${nv.matKhau}</td>
     <td>${nv.ngayLam}</td>
     <td>${nv.chucVu}</td>
     <td>${nv.tinhTongLuong()}</td>

     <td>${nv.xepLoai()}</td>

     <td>
     <button onclick = "xoaNhanVien('${
       nv.taiKhoanNv
     }')" class = "btn btn-danger mb-2"> Xóa </button>
     <button id = "suaNhanVien('${
       nv.taiKhoanNv
     }')"  class = "btn btn-waring"> Sửa </button>
     </td>
     </tr>`;
    contentHTML += trList;
  }

  document.getElementById("tableList").innerHTML = contentHTML;
}

function findAcc(id, dsnv) {
  for (var index = 0; index < dsnv.length; index++) {
    var nv = dsnv[index];
    if (nv.acc == id) {
      return index;
    }
  }

  return -1;
}

function showInfoToForm(nv) {
  document.getElementById(`TaiKhoanNV`).value = nv.taiKhoanNv;
  document.getElementById(`TenNV`).value = nv.tenNv;
  document.getElementById(`Email`).value = nv.email;
  document.getElementById(`Password`).value = nv.matKhau;
  document.getElementById(`NgayLam`).value = nv.ngayLam;
  document.getElementById(`luongCoBan`).value = nv.luongCoBan;
  document.getElementById(`chucVu`).options[
    document.getElementById(`chucVu`).selectedIndex
  ].text = nv.position;
  document.getElementById(`gioLam`).value = nv.gioLamTrongThang;
  document.getElementById(`TaiKhoanNV`).readOnly = true;
}

function resetInfoInForm() {
  document.getElementById(`TaiKhoanNV`).value = "";
  document.getElementById(`TenNV`).value = "";
  document.getElementById(`Email`).value = "";
  document.getElementById(`Password`).value = "";
  document.getElementById(`NgayLam`).value = "";
  document.getElementById(`luongCoBan`).value = "";
  document.getElementById(`chucVu`).options[
    document.getElementById(`chucVu`).selectedIndex
  ].text = "";
  document.getElementById(`gioLam`).value = "";
}
